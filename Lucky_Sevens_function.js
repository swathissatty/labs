function DiceGame(){

var startingVal = document.getElementById("startingVal").value;
var profit = 0;
var profit = startingVal; //takes the starting bet value entered by user
var firstroll = 0;
var secroll = 0;
var dice = 0;
var tally_count = 0;
var profit_array = [startingVal]; //creates array to store the money made by player each round 
var MaxVal = 0;

	do{ //ensures that while loop is ran at least once

		tally_count++;
		firstroll = Math.floor(Math.random() * 6) + 1; //Because randomizer rounds down the range would have been 0 - 5 if we had not incremented by 1
		secroll = Math.floor(Math.random() * 6) + 1;	
		dice = firstroll+secroll; //adds face value of each die

			if (dice != 7)
                        {
			profit--; //deducts profit by one
			var currentValue = profit_array[profit_array.length -1]; // retrieves most current value within array (last value pushed)
			var updatedValue = currentValue - 1; //deducts $1 from retrieved value from array
			profit_array.push(updatedValue); //takes deduction and updates array (this now becomes current value)
			tally_count = tally_count++;
			}
                        else 
                        {
                        profit += 4; 
			var currentValue = profit_array[profit_array.length -1]; 
			var updatedValue = currentValue + 4; 
			profit_array.push(updatedValue);
			}


	} 
        while(profit>0); //drops out of loop once the profit hits 0 or goes below 0 as calculated 


	MaxVal = Math.max.apply(null, profit_array); // finds the most profitable value in profit_array array
	MaxPos = profit_array.indexOf(MaxVal); // finds position when profit is at its highest in array 
	if(MaxPos<1){
		MaxPos = 0;
	}

	document.getElementById("First").innerHTML=("<center><br><br><table><th>Results</th><tr><th>Starting Bet </th><th>$" + startingVal + "</th></tr><tr><td>Total Rolls Before Going Broke</td><td>"+ tally_count +"</td></tr><tr><td>Highes Amount Won</td><td>"+MaxVal+"</td></tr><tr><td>Roll Count at Highest Amount Won</td><td>"+ MaxPos + "</td></tr></table></center>");
	document.getElementById("Second").innerHTML="Play Again";

}